import Container from "react-bootstrap/Container";
import Nav from "react-bootstrap/Nav";
import Navbar from "react-bootstrap/Navbar";
import NavDropdown from "react-bootstrap/NavDropdown";
import { Outlet, Link } from "react-router-dom";

function Header() {
  return (
    <>
      <Navbar bg="dark" expand="lg" variant="dark">
        <Container>
          <Navbar.Brand href="/">Tasks</Navbar.Brand>
          <Navbar.Toggle aria-controls="basic-navbar-nav" />
          <Navbar.Collapse id="basic-navbar-nav">
            <Nav className="me-auto">
              <Nav.Link href="/">Home</Nav.Link>
              <NavDropdown title="Task-list" id="basic-nav-dropdown">
                <NavDropdown.Item href="/task1">
                  Render Data From Static Objects
                </NavDropdown.Item>
                <NavDropdown.Item href="/task2">
                  List From Array with map()
                </NavDropdown.Item>
                <NavDropdown.Item href="/task3">
                  On text input text replication
                </NavDropdown.Item>
                <NavDropdown.Item href="/task4">
                  Price X Quantity
                </NavDropdown.Item>
                <NavDropdown.Item href="/task5">
                  onClick hide/show
                </NavDropdown.Item>
                <NavDropdown.Item href="/task7">Converters</NavDropdown.Item>
                <NavDropdown.Item href="/task6">
                  Increment/Decrement Counter
                </NavDropdown.Item>
              </NavDropdown>
            </Nav>
          </Navbar.Collapse>
        </Container>
      </Navbar>

      <Outlet />
    </>
  );
}

export default Header;
