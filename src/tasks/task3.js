import React, { useState } from "react";
import { Container } from "react-bootstrap";

export default function Task3() {
  const [inputtext, setinputtext] = useState("");

  function handleChange(e) {
    setinputtext(e.target.value);
  }

  return (
    <Container className="mt-5">
      <h1 className="text-center">Welcome to Logistic Infotech</h1>
      <input
        type="text"
        className="form-control mb-3"
        onChange={handleChange}
      ></input>
      <h3 className="text-center">{inputtext}</h3>
    </Container>
  );
}
