import React, { useState } from "react";
import { Container } from "react-bootstrap";

function Info() {
  return <p>Show/Hide text onClick</p>;
}

export default function Task5() {
  const [text, showtext] = useState(false);

  return (
    <Container className="text-center mt-5">
      <h1 className="mb-3">Welcome to Logistic Infotech</h1>
      {text ? <Info /> : null}
      <button className="btn btn-primary me-1" onClick={() => showtext(true)}>
        Show text
      </button>
      <button className="btn btn-primary" onClick={() => showtext(false)}>
        Hide text
      </button>
    </Container>
  );
}
