import React, { useState } from "react";
import { Container } from "react-bootstrap";
import Button from "react-bootstrap/Button";
import Form from "react-bootstrap/Form";
import InputGroup from "react-bootstrap/InputGroup";

export default function Task6() {
  const [element, setelement] = useState([]);

  function clone() {
    let cloned = React.cloneElement(<Iddiv />);
    setelement([...element, cloned]);
  }
  return (
    <Container className="text-center mt-5 clone">
      <Button onClick={clone}>New</Button>
      {element}
    </Container>
  );
}

function Iddiv() {
  const [count, setcount] = useState(0);
  return (
    <InputGroup className="mb-3">
      <Button
        variant="outline-secondary"
        id="button-addon2"
        onClick={() => setcount(count - 1)}
      >
        -
      </Button>
      <input type="text" value={count} />
      <Button
        variant="outline-secondary"
        id="button-addon2"
        onClick={() => setcount(count + 1)}
      >
        +
      </Button>
    </InputGroup>
  );
}
