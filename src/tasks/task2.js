import React from "react";
import { Container } from "react-bootstrap";

export default function Task2() {
  const fruits = ["Banana", "Orange", "Apple", "Mango"];

  return (
    <Container className="mt-5">
      <ul>
        {fruits.map((item) => (
          <li> {item} </li>
        ))}
      </ul>
    </Container>
  );
}
