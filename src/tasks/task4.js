import React, { useState } from "react";
import { Row, Col, Container, Form } from "react-bootstrap";

export default function Task4() {
  const [price, setprice] = useState(0);
  const [qty, setqty] = useState(0);

  function handlepriceChange(e) {
    setprice(parseInt(e.target.value));
  }

  function handleqtyChange(e) {
    setqty(parseInt(e.target.value));
  }

  return (
    <Container className="mt-5">
      {console.log("rendered")}
      <h1 className="text-center mb-3">Welcome to Logistic Infotech</h1>
      <Row className="mb-3">
        <Col>
          <Form.Label htmlFor="inputtext">Price</Form.Label>
          <Form.Control
            type="number"
            id="inputtext"
            onChange={handlepriceChange}
          />
        </Col>
        <Col>
          <Form.Label htmlFor="inputtext2">Qty.</Form.Label>
          <Form.Control
            type="number"
            id="inputtext2"
            onChange={handleqtyChange}
          />
        </Col>
      </Row>
      <h3 className="text-center">{price * qty}</h3>
    </Container>
  );
}
