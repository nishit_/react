import React from "react";
import { Container } from "react-bootstrap";

export default function Task1() {
  const person = {
    fname: "John",
    lname: "Doe",
  };

  return (
    <Container className="mt-5">
      <h3>
        Hello My Name Is {person.fname} {person.lname}
      </h3>
      <p>Above Name is Rendered From A 'person' Object</p>
    </Container>
  );
}
