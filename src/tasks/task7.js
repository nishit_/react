import React, { useState } from "react";
import { Container } from "react-bootstrap";
import { Dropdown } from "react-bootstrap";
import { InputGroup } from "react-bootstrap";
import { Form } from "react-bootstrap";

export default function Task7() {
  const [converter, setconverter] = useState("time");

  return (
    <Container className="text-center mt-5">
      <Dropdown className="mb-3">
        <Dropdown.Toggle id="dropdown-basic">Converter</Dropdown.Toggle>
        <Dropdown.Menu>
          <Dropdown.Item onClick={() => setconverter("time")}>
            Time
          </Dropdown.Item>
          <Dropdown.Item onClick={() => setconverter("temp")}>
            Temprature
          </Dropdown.Item>
        </Dropdown.Menu>
      </Dropdown>
      {converter == "time" ? <Timeconverter /> : <Tempconverter />}
    </Container>
  );
}

function Timeconverter() {
  const [unit, setunit] = useState("second");
  const [value, setvalue] = useState(0);

  function converttomin(e) {
    setunit("second");
    setvalue(e.target.value);
  }

  function converttosec(e) {
    setunit("minute");
    setvalue(e.target.value);
  }

  const sec = unit == "second" ? value : value * 60;
  const min = unit == "minute" ? value : value / 60;

  return (
    <>
      <InputGroup className="mb-3">
        <Form.Control
          aria-describedby="second"
          onChange={converttomin}
          value={sec}
        />
        <InputGroup.Text id="second">Seconds</InputGroup.Text>
      </InputGroup>
      <InputGroup className="mb-3">
        <Form.Control
          aria-describedby="minute"
          onChange={converttosec}
          value={min}
        />
        <InputGroup.Text id="minute">Minutes</InputGroup.Text>
      </InputGroup>
    </>
  );
}

function Tempconverter() {
  const [scale, setscale] = useState("celsius");
  const [temp, settemp] = useState(0);

  function converttof(e) {
    setscale("celsius");
    settemp(e.target.value);
  }

  function converttoc(e) {
    setscale("farenhit");
    settemp(e.target.value);
  }

  const celsius = scale == "celsius" ? temp : ((temp - 32) * 5) / 9;
  const farenhit = scale == "farenhit" ? temp : (temp * 9) / 5 + 32;

  return (
    <>
      <InputGroup className="mb-3">
        <Form.Control
          aria-describedby="celsius"
          onChange={converttof}
          value={celsius}
        />
        <InputGroup.Text id="celsius">Celsius</InputGroup.Text>
      </InputGroup>
      <InputGroup className="mb-3">
        <Form.Control
          aria-describedby="farenhit"
          onChange={converttoc}
          value={farenhit}
        />
        <InputGroup.Text id="farenhit">Farenhit</InputGroup.Text>
      </InputGroup>
    </>
  );
}
